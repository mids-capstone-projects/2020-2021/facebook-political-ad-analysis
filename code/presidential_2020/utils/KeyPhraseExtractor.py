from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential


class KeyPhraseExtractor:

    def __init__(self):
        self.ta_credential = AzureKeyCredential("<<AZURE APP CREDENTIAL>>")
        self.client = TextAnalyticsClient(
            endpoint = "<<AZURE APP ENDPOINT>>",
            credential = self.ta_credential 
        )

    def extract(self, text, lang="en"):
        """
        Function to extract key phrases from the text
        Input:
            text: (string); String from which key phrases is to be extracted
            lang: (string); Language code of the input string. Defualt 'en'
        """
        try:
            documents = [{
                            'text': text,
                            'id': 1,
                            'language': lang
                        }]
            response = self.client.extract_key_phrases(documents = documents)[0]

            if not response.is_error:
                result = []
                for kp in response.key_phrases:
                    if not kp.isalpha() and len(kp) > 2:
                        result.append(kp.lower())
                return list(set(result))
            else:
                print(response.id, response.error)
        except Exception as err:
            print(f"Encountered exception: {err}")

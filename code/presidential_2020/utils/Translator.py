import os, requests, uuid, json

class cust_Translate:
  def __init__(self):
    self.subscription_key = "<<AZURE APP CREDENTIAL>>"
    self.endpoint = 'https://api.cognitive.microsofttranslator.com'
    self.region = '<<AZURE APP REGION>>'
    self.headers  = {
        'Ocp-Apim-Subscription-Key': self.subscription_key,
        'Ocp-Apim-Subscription-Region': self.region,
        'Content-type': 'application/json',
        'X-ClientTraceId': str(uuid.uuid4())
        }

  def detect(self, text):
    path = '/detect?api-version=3.0'
    constructed_url = self.endpoint + path
    body = [{'text':text}]

    request = requests.post(constructed_url, headers=self.headers, json=body)
    response = request.json()
   
    return response[0]['language']

  def translate(self, text, source = "es", target = "en"):
   
    if source == 'en':
      return text

    path = '/translate?api-version=3.0'
    params = f"&from={source}&to={target}"
    constructed_url = self.endpoint + path + params

    body = [{'text':text}]

    request = requests.post(constructed_url, headers=self.headers, json=body)
    response = request.json()

    return response[0]['translations'][0]['text']

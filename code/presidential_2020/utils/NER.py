from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential

class NER():

  def __init__(self):
    self.ta_credential = AzureKeyCredential("<<AZURE APP CREDENTIAL>>")
    self.client = TextAnalyticsClient(
        endpoint = "<<AZURE APP ENDPOINT>>",
        credential = self.ta_credential
      )

  def get_ne(self, text):
    try:
      documents = [text]
      result = self.client.recognize_entities(documents)[0]
      all_entities = list(set([(entity.text, entity.category) 
                      for entity in result.entities\
                      if entity.category in ["Person","Event","Product"]]))
      return all_entities
    except Exception as err:
      print(f"Encountered exception. {err}")

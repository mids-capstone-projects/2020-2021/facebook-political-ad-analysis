# Facebook Political Ads Analysis 
****
## Duke MIDS 2021
****

Welcome to our repository for the Duke MIDS 2020-2021 capstone project analyzing 2020 presidential ads run on Facebook. We explored a brand new dataset of Facebook political ads from recent US elections. The overall goal was a deep dive into modern political campaigning on social media. Observers have speculated that political ads on social media contribute to growing divisions in American society, engaging in partisan mudslinging and uncivil attacks over a respectful discussion of the issues. No one, to date, has tested this empirically. Using a complete universe of political ads from Facebook, we  evaluated their content, tone, and targets to get a better sense of the kinds of political messages that US voters may encounter online.

This repo contains the following items:
* We used [Facebook Ad library](https://www.facebook.com/ads/library/?active_status=all&ad_type=all&country=US) which is an online archive of all political ads run on Facebook since May 2018. 
* Our final dataset contained 484,392 campaign ads run by Donald Trump and Joe Biden on Facebook between April 1st and November 3rd (Election Day), 2020
* Sample of text data from Facebook ads run by Joe Biden and Donald Trump during the 2020 US election [HERE](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/facebook-political-ad-analysis/-/blob/master/Sample_of_2020_Presidential_Facebook_Ads.csv).

We used various techniques such as Named Entity Recognition (NER), keyphrase extraction, sentiment analysis to analyze Facebook ads across five moderators:
* Candidate
* Gaol of the ad (donation request, vote request, etc.)
* Timing of the ad (beginning phase, end phase of campaign)
* Issue content of the ad (Social, economic, etc.)
* Geographic targeting

The full project report and presentation can be found [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/facebook-political-ad-analysis/-/blob/master/reports/whitepaper_presidential_2020.pdf) and [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/facebook-political-ad-analysis/-/blob/master/presentations/presidential_2020.pptx)


